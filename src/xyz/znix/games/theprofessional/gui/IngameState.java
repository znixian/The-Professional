/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.gui;

import xyz.znix.games.theprofessional.levels.Level;
import xyz.znix.games.theprofessional.objects.ObjectWindow;
import java.util.LinkedList;
import java.util.List;
import xyz.znix.games.theprofessional.objects.ObjectElevator;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import xyz.znix.games.theprofessional.levels.Objective;
import xyz.znix.games.theprofessional.objects.BasicObject;
import xyz.znix.games.theprofessional.objects.BasicObjectList;
import xyz.znix.games.theprofessional.objects.ObjectCrowd;
import xyz.znix.games.theprofessional.objects.ObjectPlayer;
import xyz.znix.games.theprofessional.objects.ObjectStairwell;
import xyz.znix.games.theprofessional.objects.ObjectValuable;
import xyz.znix.games.theprofessional.objects.npc.ObjectNPCEnemy;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.State;
import xyz.znix.slickengine.impl.TiledMapImpl;
import xyz.znix.slickengine.objects.ObjectList;
import xyz.znix.slickengine.tiled.Layer;
import xyz.znix.slickengine.tiled.MapObj;
import xyz.znix.slickengine.tiled.Tile;
import xyz.znix.slickengine.tiled.TiledMap;

/**
 *
 * @author Campbell Suter
 */
public class IngameState implements State {

    private static final Color SKY = new Color(5, 5, 30);
    private static final String DONE_TEXT = "All objectives complete.";

    private TiledMap map;
    private Layer solidObjects;
    private BasicObjectList objects;
    private SpriteSheet sheet;
    private Context context;
    private final Level level;
    private ObjectPlayer player;
    private ObjectPlayer framePlayer;

    private int msSinceLastUpdate;

    private int followX, followY;

    public IngameState(Level level) {
        this.level = level;
    }

    @Override
    public void init(Context context) throws SlickException {
        map = level.loadMap();
        objects = new BasicObjectList();

        solidObjects = map.getLayer("Solid Objects");

        setupObjects(context);

        Image image = ((TiledMapImpl) map).getRawMap().getTileSets().
                get(0).getTileSetImage();
        image.setFilter(Image.FILTER_NEAREST);
        sheet = new SpriteSheet(image, 32, 32);
    }

    private void setupObjects(Context context) throws SlickException {
        map.processObjectType("Elevator", (obj) -> {
            BasicObject wobj = new ObjectElevator(this, obj);
            applyObjectSettings(obj, wobj);
            return true;
        });

        map.processObjectType("Stairwell", (obj) -> {
            BasicObject wobj = new ObjectStairwell(obj, this);
            applyObjectSettings(obj, wobj);
            return true;
        });

        map.processObjectType("Window", (obj) -> {
            BasicObject wobj = new ObjectWindow(this, obj);
            applyObjectSettings(obj, wobj);
            return true;
        });

        map.processObjectType("ElevatorController", (obj) -> {
            BasicObject wobj = new ObjectElevatorController(this, obj);
            applyObjectSettings(obj, wobj);
            return true;
        });

        map.processObjectType("Valuable", (obj) -> {
            BasicObject wobj = new ObjectValuable(this, obj);
            applyObjectSettings(obj, wobj);
            return true;
        });

        map.processObjectType("Crowd", (obj) -> {
            BasicObject wobj = new ObjectCrowd(this, obj);
            applyObjectSettings(obj, wobj);
            return true;
        });

        map.processObjectType("Player", (obj) -> {
            BasicObject wobj = new ObjectPlayer(this, obj);
            applyObjectSettings(obj, wobj);
            return true;
        });

        map.processObjectType("NPCEnemy", (obj) -> {
            BasicObject wobj = new ObjectNPCEnemy(this, obj);
            applyObjectSettings(obj, wobj);
            return true;
        });

        for (BasicObject object : objects) {
            object.init(context);
        }

        objects.sortByDepth();
    }

    private void applyObjectSettings(MapObj obj, BasicObject wobj) {
        objects.add(wobj);
        wobj.setX((int) obj.getX());
        wobj.setY((int) (obj.getY() - obj.getHeight()));
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        msSinceLastUpdate += delta;
        int minFrameLength = 1000 / 60;
        if (msSinceLastUpdate < minFrameLength) {
            return;
        }
        msSinceLastUpdate %= minFrameLength;

        framePlayer = null;
        objects.update(context, delta);
        player = framePlayer;
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        this.context = context;
        g.setBackground(SKY);
        g.clear();

        int camX = getCamX();
        int camY = getCamY();

        g.pushTransform();
        g.translate(-camX, -camY);

        map.render(g, 0, 0);
        objects.render(context, g);

        g.popTransform();

        drawObjectives(g);
    }

    public SpriteSheet getSheet() {
        return sheet;
    }

    public TiledMap getMap() {
        return map;
    }

    public Layer getSolidObjects() {
        return solidObjects;
    }

    public ObjectList<BasicObject> getObjects() {
        return objects;
    }

    public void setFollowX(int followX) {
        this.followX = followX;
    }

    public void setFollowY(int followY) {
        this.followY = followY;
    }

    public Level getLevel() {
        return level;
    }

    public List<BasicObject> raytrace(
            int xStart, int yStart,
            int xStop, int yStop,
            BasicObject... excluding) {
        LinkedList<BasicObject> hits = new LinkedList<>();

        int total = Math.abs(xStop - xStart) + Math.abs(yStop - yStart);
        double stepX = 1.0 * (xStop - xStart) / total;
        double stepY = 1.0 * (yStop - yStart) / total;

        int lastCellX = -1;
        int lastCellY = -1;

        for (int i = 0; i < total; i++) {
            int px = (int) (i * stepX) + xStart;
            int py = (int) (i * stepY) + yStart;

            int cellX = px / 32;
            int cellY = py / 32;

            if (lastCellX != cellX || lastCellY != cellY) {
                Tile tile = solidObjects.getTileAt(cellX, cellY);
                if (tile != null) {
                    break;
                }
            }

            lastCellX = cellX;
            lastCellY = cellY;

            findTouching:
            for (BasicObject object : objects) {
                if (object.isInside(px, py)) {
                    for (BasicObject basicObject : excluding) {
                        if (basicObject == object) {
                            continue findTouching;
                        }
                    }
                    if (!hits.contains(object)) {
                        hits.add(object);
                    }
                }
            }
        }

        return hits;
    }

    /**
     * Raytrace, colliding only with walls. This is a <i>very</i> fast raytrace,
     * and takes around 150&mu;s
     *
     * @param xStart
     * @param yStart
     * @param xStop
     * @param yStop
     * @return Are there any walls between the start and stop positions
     */
    public boolean raytraceWalls(
            int xStart, int yStart,
            int xStop, int yStop) {
        int total = Math.abs(xStop - xStart) + Math.abs(yStop - yStart);
        double stepX = 1.0 * (xStop - xStart) / total;
        double stepY = 1.0 * (yStop - yStart) / total;

        int lastCellX = -1;
        int lastCellY = -1;

        for (int i = 0; i < total; i++) {
            int px = (int) (i * stepX) + xStart;
            int py = (int) (i * stepY) + yStart;

            int cellX = px / 32;
            int cellY = py / 32;

            if (lastCellX != cellX || lastCellY != cellY) {
                Tile tile = solidObjects.getTileAt(cellX, cellY);
                if (tile != null) {
                    return true;
                }
            }

            lastCellX = cellX;
            lastCellY = cellY;
        }

        return false;
    }

    public int getCamX() {
        return followX - context.getWidth() / 2;
    }

    public int getCamY() {
        return followY - context.getHeight() / 2;
    }

    public void eventOccured(Event event) {
        level.eventOccured(event);
    }

    private void drawObjectives(Graphics g) {
        // variables

        // y counter
        int y = 20;

        // how much padding to use
        int padding = 10;

        // what should the width of the box be
        int width = 0;

        // the max line height
        int lineHeight = g.getFont().getLineHeight();

        // get a list of the objectives
        List<Objective> objectives = level.getObjective();

        int listLength = objectives.size();

        // find the box's width
        if (listLength == 0) {
            width = g.getFont().getWidth(DONE_TEXT);
            listLength = 1;
        }
        for (Objective objective : objectives) {
            width = Math.max(width,
                    g.getFont().getWidth(objective.getHUDText(this)));
        }

        // apply padding
        width += padding * 2;

        // draw the box
        g.setColor(Color.black);
        g.fillRect(
                0, 0, // x,y
                width, // already got this
                y // y to start at
                + listLength * lineHeight // the height of the text
                + padding * 2 // padding at the top and bottom
        );

        g.setColor(Color.white);
        if (objectives.isEmpty()) {
            g.drawString(DONE_TEXT, padding, y);
        }
        for (Objective objective : objectives) {
            g.drawString(objective.getHUDText(this), padding, y);
            y += lineHeight;
        }
    }

    public ObjectPlayer getPlayer() {
        return player;
    }

    public void registerPlayerForFrame(ObjectPlayer framePlayer) {
        this.framePlayer = framePlayer;
    }

    public SlickEngine getEngine() {
        return context.getEngine();
    }

    public static interface Event {
    }
}
