/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.gui;

import java.awt.Font;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;
import org.w3c.dom.Document;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter
 */
public class TitleScreenState extends GUIState {

    private TrueTypeFont font;
    private Document doc;

    @Override
    public void init(Context context) throws SlickException {
        super.init(context);
        try {
            readDataFile();
        } catch (Exception ex) {
            throw new SlickException("Error loading data.xml", ex);
        }

        Font awtfont = new Font("SansSerif", Font.BOLD, 32);
        font = new TrueTypeFont(awtfont, true);

        buttons = new ArrayList<>();
        buttons.add(new Button(1, "Play", () -> {
            engine.addState(new LevelSelectState(this));
        }));
        buttons.add(new Button(2, "Quit", () -> {
            engine.removeState();
        }));
    }

    private void readDataFile()
            throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        //builder.setErrorHandler(new XMLErrorHandler());
        doc = builder.parse(ResourceLoader.getResourceAsStream("assets/data.xml"), ".");
        doc.getDocumentElement().normalize();
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        g.setBackground(Color.black);
        g.clear();

        g.setColor(Color.white);
        String title = "The Professional";

        int titleWidth = font.getWidth(title);
        int titleHeight = font.getHeight(title);

        font.drawString(
                context.getWidth() / 2 - titleWidth / 2,
                context.getHeight() / 2 - titleHeight / 2,
                title
        );

        super.render(context, g);
    }

    public Document getDoc() {
        return doc;
    }
}
