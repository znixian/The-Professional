/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.games.theprofessional.gui;

import java.util.List;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.MouseListener;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.State;

/**
 *
 * @author Campbell Suter
 */
public abstract class GUIState implements State, MouseListener {

    protected SlickEngine engine;
    protected Context context;
    protected List<Button> buttons;

    @Override
    public void init(Context context) throws SlickException {
        this.context = context;
        engine = context.getEngine();
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        for (Button button : buttons) {
            button.render(context, g);
        }
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        if (button == 0) {
            for (Button b : buttons) {
                if (b.isMouseInside()) {
                    b.action.run();
                }
            }
        }
    }

    public class Button {

        public int index;
        public String label;
        public Runnable action;

        public Button(int index, String label, Runnable action) {
            this.index = index;
            this.label = label;
            this.action = action;
        }

        public int getX() {
            return context.getWidth() / 4;
        }

        public int getY() {
            int baseHeight = context.getHeight() / 2 - getHeight() / 2;
            int seperator = getHeight() + getHeight() / 2;
            return baseHeight + seperator * index;
        }

        public int getWidth() {
            return context.getWidth() / 2;
        }

        public int getHeight() {
            return 40;
        }

        public boolean isMouseInside() {
            return isInside(Mouse.getX(), context.getHeight() - Mouse.getY());
        }

        public boolean isInside(int x, int y) {
            int tx = getX();
            int ty = getY();

            return x >= tx && y >= ty
                    && x < tx + getWidth()
                    && y < ty + getHeight();
        }

        public void render(Context context, Graphics g) throws SlickException {
            boolean mouseInside = isMouseInside();

            g.setColor(Color.white);
            if (mouseInside) {
                g.drawRect(getX(), getY(), getWidth(), getHeight());
            } else {
                g.fillRect(getX(), getY(), getWidth(), getHeight());
            }
            
            Font font = g.getFont();

            int width = font.getWidth(label);
            int height = font.getHeight(label);

            int x = getWidth() / 2 - width / 2;
            int y = getHeight() / 2 - height / 2;

            g.setColor(mouseInside ? Color.white : Color.black);
            g.drawString(label, getX() + x, getY() + y);
        }
    }
}
