/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.gui;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import xyz.znix.games.theprofessional.levels.Level;
import xyz.znix.games.theprofessional.levels.XMLLevel;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter
 */
public class LevelSelectState extends GUIState {

    private NodeList mapList;
    private final TitleScreenState titleScreen;

    public LevelSelectState(TitleScreenState titleScreen) {
        this.titleScreen = titleScreen;
    }

    @Override
    public void init(Context context) throws SlickException {
        super.init(context);

        buttons = new ArrayList<>();
        buttons.add(new Button(1, "Back", () -> {
            engine.removeState();
//                Level level = new AssassinateLevel("BigBadGuy");
//                engine.addState(new IngameState(level));
        }));

        readLevelFile();
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        g.setBackground(Color.black);
        g.clear();

        g.setColor(Color.white);
        String title = "Level Select";

        Font font = g.getFont();
        int titleWidth = font.getWidth(title);
        int titleHeight = font.getHeight(title);

        font.drawString(
                context.getWidth() / 2 - titleWidth / 2,
                context.getHeight() / 2 - titleHeight / 2,
                title
        );

        super.render(context, g);
    }

    private void readLevelFile() {
        mapList = titleScreen.getDoc().getElementsByTagName("map");

        for (int i = 0; i < mapList.getLength(); i++) {
            Element map = (Element) mapList.item(i);

            String filename = map.getAttribute("filename");
            String name = map.getAttribute("name");

            buttons.add(new HalfButton(2 + i / 2, i % 2 == 0, name, () -> {
                Level level = new XMLLevel(
                        filename,
                        map.getElementsByTagName("objective")
                );
                engine.addState(new IngameState(level));
            }));

        }
    }

    public class HalfButton extends Button {

        public final boolean firstHalf;

        public HalfButton(int index, boolean firstHalf, String label, Runnable action) {
            super(index, label, action);
            this.firstHalf = firstHalf;
        }

        public int getCentralMargin() {
            return 20;
        }

        @Override
        public int getWidth() {
            return super.getWidth() / 2 - getCentralMargin() / 2;
        }

        @Override
        public int getX() {
            if (firstHalf) {
                return super.getX();
            } else {
                return super.getX() + getWidth() + getCentralMargin();
            }
        }
    }
}
