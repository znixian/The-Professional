/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.games.theprofessional.gui;

import java.util.Optional;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.objects.BasicObject;
import xyz.znix.games.theprofessional.objects.ObjectElevatorCart;
import xyz.znix.games.theprofessional.objects.ObjectPlayer;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectElevatorController extends BasicObject {

    private final String system;
    private ObjectPlayer player;
    private ObjectElevatorCart elevator;
    private boolean keyDown;

    public ObjectElevatorController(IngameState state, MapObj obj) {
        super(state, obj);
        this.system = obj.getProperties().getProperty("system");
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        if (player != null) {
            if (Keyboard.isKeyDown(Keyboard.KEY_A)
                    || Keyboard.isKeyDown(Keyboard.KEY_D)) {
                player.setX(getX());
                player.setY(getY());
                state.getObjects().add(player);
                player = null;
            }

            boolean wasKeyDown = keyDown;
            keyDown = false;
            if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
                if (!wasKeyDown) {
                    // w pressed
                    elevator.setLightsEnabled(false);
                }
                keyDown = true;
            }

            // update the follow cam
            state.setFollowX(elevator.getX());
            state.setFollowY(elevator.getY());
        }
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        state.getSheet().getSubImage(1, 3).draw((int) x, (int) y);
        if (player != null) {
            state.getSheet().getSubImage(3, 1).draw((int) x, (int) y);
        }
    }

    @Override
    public boolean onInteract(ObjectPlayer player) {
        Optional<ObjectElevatorCart> elev = state.getObjects().stream()
                .filter((o) -> (o instanceof ObjectElevatorCart))
                .map((t) -> (ObjectElevatorCart) t)
                .filter((t) -> t.getSystem().equals(system))
                .findAny();

        if (!elev.isPresent()) {
            return true;
        }

        this.player = player;
        elevator = elev.get();
        state.getObjects().remove(player);
        return true;
    }

}
