/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.State;

/**
 *
 * @author Campbell Suter
 */
public class LoadingState implements State {

    @Override
    public void update(Context context, int delta) throws SlickException {
        TitleScreenState state = new TitleScreenState();
        context.getEngine().removeState();
        context.getEngine().addState(state);
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        g.setBackground(Color.black);
        g.clear();

        g.setColor(Color.white);
        String title = "Loading";

        Font font = g.getFont();
        int titleWidth = font.getWidth(title);
        int titleHeight = font.getHeight(title);
        
        font.drawString(
                context.getWidth() / 2 - titleWidth / 2,
                context.getHeight() / 2 - titleHeight / 2,
                title
        );
    }
    
}
