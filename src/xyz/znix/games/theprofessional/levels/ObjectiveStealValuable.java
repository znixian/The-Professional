/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.games.theprofessional.levels;

import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.games.theprofessional.objects.ObjectValuable;

/**
 *
 * @author Campbell Suter
 */
public class ObjectiveStealValuable implements Objective {

    private final String name;

    public ObjectiveStealValuable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean eventOccured(IngameState.Event event) {
        if (!(event instanceof ObjectValuable.ValuableStolenEvent)) {
            return false;
        }

        ObjectValuable.ValuableStolenEvent e
                = (ObjectValuable.ValuableStolenEvent) event;
        return name.equals(e.getName());
    }

    @Override
    public String getHUDText(IngameState state) {
        return "Steal " + name;
    }

}
