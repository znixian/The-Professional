/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.levels;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author Campbell Suter
 */
public class XMLLevel extends BasicLevel {

    private final List<Objective> objectives;

    public XMLLevel(String filename, NodeList XMLobjectives) {
        super(filename);
        objectives = new ArrayList<>();

        for (int i = 0; i < XMLobjectives.getLength(); i++) {
            Element objElem = (Element) XMLobjectives.item(i);
            switch (objElem.getAttribute("type")) {
                case "KillNPC":
                    objectives.add(new ObjectiveKillNPC(objElem.getTextContent()));
                    break;
                case "StealValuable":
                    objectives.add(new ObjectiveStealValuable(objElem.getTextContent()));
                    break;
                default:
                    throw new IllegalArgumentException(objElem.getTagName());
            }
        }
    }

    @Override
    public List<Objective> getObjective() {
        return objectives;
    }

}
