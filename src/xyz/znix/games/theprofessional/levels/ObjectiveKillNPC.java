/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.levels;

import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.games.theprofessional.objects.npc.ObjectNPCEnemy;

/**
 *
 * @author Campbell Suter
 */
public class ObjectiveKillNPC implements Objective {

    private final String name;

    public ObjectiveKillNPC(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean eventOccured(IngameState.Event event) {
        if (!(event instanceof ObjectNPCEnemy.NPCDiedEvent)) {
            return false;
        }

        ObjectNPCEnemy.NPCDiedEvent e = (ObjectNPCEnemy.NPCDiedEvent) event;
        return name.equals(e.getNpc().getName());
    }

    @Override
    public String getHUDText(IngameState state) {
        return "Kill " + name;
    }

}
