/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.levels;

import java.util.Iterator;
import java.util.List;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.tiled.TiledMap;

/**
 *
 * @author Campbell Suter
 */
public interface Level {
    List<Objective> getObjective();
    
    TiledMap loadMap() throws SlickException;

    default void eventOccured(IngameState.Event event) {
        for (Iterator<Objective> it = getObjective().iterator(); it.hasNext();) {
            boolean handled = it.next().eventOccured(event);
            if(handled) {
                it.remove();
            }
        }
    }

    default boolean isFinished() {
        return getObjective().isEmpty();
    }
}
