/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.levels;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Campbell Suter
 */
public class AssassinateLevel extends BasicLevel {

    private final List<Objective> objectives;

    public AssassinateLevel(String filename, String... names) {
        super(filename);
        objectives = new ArrayList<>();

        for (String name : names) {
            objectives.add(new ObjectiveKillNPC(name));
        }
    }

    @Override
    public List<Objective> getObjective() {
        return objectives;
    }

}
