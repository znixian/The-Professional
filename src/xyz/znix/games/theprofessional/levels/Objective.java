/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.levels;

import xyz.znix.games.theprofessional.gui.IngameState;

/**
 *
 * @author Campbell Suter
 */
public interface Objective {

    /**
     * Alert this objective that this event occurred. Return true if this event
     * completes this objective.
     *
     * @param event The event that happened
     * @return Is this event completed
     */
    boolean eventOccured(IngameState.Event event);
    
    /**
     * Get the label that should appear on the HUD when in game.
     * @param state The state drawing the screen
     * @return The text to draw
     */
    String getHUDText(IngameState state);
}
