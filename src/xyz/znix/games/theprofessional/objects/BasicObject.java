/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.objects.WObject;
import xyz.znix.slickengine.tiled.Layer;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public abstract class BasicObject implements WObject {

    protected final IngameState state;
    protected final String name;

    public BasicObject(IngameState state, MapObj obj) {
        this.state = state;
        name = obj.getName();
    }

    public BasicObject(IngameState state, String name) {
        this.state = state;
        this.name = name;
    }

    public void init(Context context) throws SlickException {
    }

    protected double x, y;

    @Override
    public int getX() {
        return (int) x;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return (int) y;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public int getWidth() {
        return 32;
    }

    @Override
    public int getHeight() {
        return 32;
    }

    public int getCenterX() {
        return getX() + getWidth() / 2;
    }

    public int getCenterY() {
        return getY() + getHeight() / 2;
    }

    public String getName() {
        return name;
    }

    public int moveAtMaxX(int maxMovement) {
        if (maxMovement == 0) {
            return 0;
        }

        int dir = maxMovement / Math.abs(maxMovement);

        if (!canMoveTo(getX() + dir, getY())) {
            return 0;
        }

        while (!canMoveTo(getX() + maxMovement, getY())) {
            if (maxMovement > 0) {
                maxMovement--;
            } else {
                maxMovement++;
            }
        }

        return maxMovement;
    }

    public boolean isOverlapping(BasicObject o) {
        return isOverlapping(this, o);
    }

    public boolean isOverlapping(BasicObject o, int newx, int newy) {
        return isOverlapping(this, o, newx, newy);
    }

    public static boolean isOverlapping(BasicObject o1, BasicObject o2) {
        return isOverlapping(o1, o2, o1.getX(), o1.getY());
    }

    /**
     * Returns true if two objects overlap
     *
     * @param o1
     * @param o2
     * @param newx
     * @param newy
     * @return
     */
    public static boolean isOverlapping(BasicObject o1, BasicObject o2, int newx, int newy) {
        // If one rectangle is on left side of other
        if (newx > o2.getX() + o2.getWidth()
                || o2.getX() > newx + o1.getWidth()) {
            return false;
        }

        // If one rectangle is above other
        return !(newy > o2.getY() + o2.getHeight()
                || o2.getY() > newy + o1.getHeight());
    }

    public int moveAtMaxY(int maxMovement) {
        if (maxMovement == 0) {
            return 0;
        }

        int dir = maxMovement / Math.abs(maxMovement);

        if (!canMoveTo(getX(), getY() + dir)) {
            return 0;
        }

        while (!canMoveTo(getX(), getY() + maxMovement)) {
            if (maxMovement > 0) {
                maxMovement--;
            } else {
                maxMovement++;
            }
        }

        return maxMovement;
    }

    public boolean canMoveTo(int x, int y) {
        for (BasicObject object : state.getObjects()) {
            if (!object.canMoveThrough(this)) {
                if (isOverlapping(object, x, y)) {
                    object.collideWith(this);
                    return false;
                }
            }
        }

        return checkTileLocationEmpty(
                (x / getWidth()),
                (y / getHeight())
        ) && checkTileLocationEmpty(
                (x / getWidth()),
                (int) Math.ceil(1.0 * y / getHeight())
        ) && checkTileLocationEmpty(
                (int) Math.ceil(1.0 * x / getWidth()),
                (int) Math.ceil(1.0 * y / getHeight())
        ) && checkTileLocationEmpty(
                (int) Math.ceil(1.0 * x / getWidth()),
                (y / getHeight())
        );
    }

    public boolean checkTileLocationEmpty(int x, int y) {
        Layer solidObjects = state.getSolidObjects();
        return solidObjects.getTileAt(x, y) == null;
    }

    public boolean onInteract(ObjectPlayer player) {
        return false;
    }

    public void onShoot(ObjectPlayer.ShotResult result) {
    }

    public boolean canSeeThrough(BasicObject object) {
        return true;
    }

    public boolean canMoveThrough(BasicObject object) {
        return true;
    }

    public void collideWith(BasicObject object) {
    }

    public void onRemove() {
    }

    /**
     * The depth of this object. Lower values appear in front. Defaults to
     * {@code 0}.
     *
     * @return The depth of this object.
     */
    public int getDepth() {
        return 0;
    }
}
