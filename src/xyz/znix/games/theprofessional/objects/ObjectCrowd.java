/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectCrowd extends BasicObject {

    private int timer;
    private Map<BasicObject, ObjectCrowdWandering> targets;

    public ObjectCrowd(IngameState state, MapObj obj) {
        super(state, obj);
    }

    @Override
    public void init(Context context) throws SlickException {
        super.init(context);

        targets = new HashMap<>();

        Iterator<BasicObject> targetsIte = state.getObjects().stream()
                .filter((e) -> (e instanceof ObjectElevator))
                .filter((e) -> (e.getY() + e.getHeight() == getY() + getHeight()))
                .iterator();

        for (; targetsIte.hasNext();) {
            BasicObject target = targetsIte.next();
            if (target instanceof ObjectElevator) {
                ((ObjectElevator) target).registerCrowd(this);
            }
            targets.put(target, null);
        }
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        timer++;
        if (timer >= 60) {
            timer = 0;

            Map.Entry<BasicObject, ObjectCrowdWandering> set;
            for (Map.Entry<BasicObject, ObjectCrowdWandering> entry : targets.entrySet()) {
                BasicObject target = entry.getKey();
                if (entry.getValue() != null) {
                    continue;
                }
                ObjectCrowdWandering obj
                        = new ObjectCrowdWandering(state, target, "",
                                () -> {
                                    targets.put(target, null);
                                });
                obj.setX(getX());
                obj.setY(getY());
                entry.setValue(obj);
                state.getObjects().add(obj);
                break;
            }
        }
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        state.getSheet().getSubImage(0, 3).draw((int) x, (int) y);
    }

    public void cartArrived(ObjectElevator elevator) {
        if (!elevator.getCart().hasCrowd()) {
            return;
        }
        ObjectCrowdWandering obj
                = new ObjectCrowdWandering(state, this, "", null);
        obj.setX(elevator.getX() + 32);
        obj.setY(elevator.getY() + elevator.getHeight() - obj.getHeight());
        if (!elevator.getCart().isLightsEnabled()) {
            elevator.getCart().setHasCrowd(false);
        }
        state.getObjects().add(obj);
    }
}

class ObjectCrowdWandering extends PhysicsObject {

    private final BasicObject target;
    private final Runnable onRemove;

    public ObjectCrowdWandering(IngameState state, BasicObject target,
            String name, Runnable onRemove) {
        super(state, name);
        this.target = target;
        this.onRemove = onRemove;
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        super.update(context, delta);
        int xdiff = target.getX() - getX();
        int maxSpeed = 1;
        if (xdiff > maxSpeed) {
            xdiff = maxSpeed;
        } else if (xdiff < -maxSpeed) {
            xdiff = -maxSpeed;
        }
        if (xdiff == 0) {
            boolean done = false;
            if (target instanceof ObjectElevator
                    && ((ObjectElevator) target).getCart() != null
                    && ((ObjectElevator) target).getCart().isLightsEnabled()
                    && ((ObjectElevator) target).getCart().areDoorsOpen()
                    ) {
                ((ObjectElevator) target).getCart().setHasCrowd(true);
                done = true;
            }
            if (target instanceof ObjectCrowd) {
                done = true;
            }
            if (done) {
                state.getObjects().remove(this);
            }
            return;
        }
        x += moveAtMaxX(xdiff);
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        state.getSheet().getSubImage(0, 3).draw((int) x, (int) y);
    }

    @Override
    public void onRemove() {
        if (onRemove != null) {
            onRemove.run();
        }
        super.onRemove();
    }
}
