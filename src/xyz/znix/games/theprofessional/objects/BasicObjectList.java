/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.games.theprofessional.objects;

import java.util.Collections;
import java.util.Comparator;
import xyz.znix.slickengine.objects.ObjectList;

/**
 *
 * @author Campbell Suter
 */
public class BasicObjectList extends ObjectList<BasicObject> {

    @Override
    public boolean remove(BasicObject o) {
        o.onRemove();
        return super.remove(o);
    }

    public void sortByDepth() {
        Collections.sort(objs, new Comparator<BasicObject>() {
            @Override
            public int compare(BasicObject o1, BasicObject o2) {
                return o2.getDepth() - o1.getDepth();
            }
        });
    }

}
