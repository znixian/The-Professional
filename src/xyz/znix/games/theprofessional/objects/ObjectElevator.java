/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.GUtils;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectElevator extends BasicObject {

    private final String system;
    private final int floor;
    private final boolean escapeElevator;
    private ObjectElevatorCart cart;
    private final ArrayList<ObjectCrowd> crowds;

    public ObjectElevator(IngameState state, MapObj obj) {
        super(state, obj);
        crowds = new ArrayList<>();
        system = obj.getProperties().getProperty("system");
        floor = Integer.parseInt(obj.getProperties().getProperty("floor"));
        escapeElevator = Objects.equals("true", obj.getProperties().getProperty("escape"));
        boolean hasCart = Objects.equals("true", obj.getProperties().getProperty("hasCart"));

        if (hasCart) {
            state.getObjects().add(new ObjectElevatorCart(state, system, this));
        }
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        if (cart == null || !cart.areDoorsOpen()) {
            GUtils.drawMultitileSprite(this, state.getSheet(), 2, 3);
        }
        if (escapeElevator && state.getLevel().isFinished()) {
            state.getSheet().getSubImage(2, 2).draw((int) x, (int) y, 2);
        }
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
    }

    public ObjectElevator getElevatorAtFloor(int floor) {
        Optional<ObjectElevator> obj = state.getObjects().stream()
                .filter((o) -> (o instanceof ObjectElevator))
                .map((t) -> (ObjectElevator) t)
                .filter((t) -> t.system.equals(system))
                .filter((t) -> t.floor == floor).findAny();
        return obj.orElse(null);
    }

    @Override
    public int getHeight() {
        return 64;
    }

    @Override
    public int getWidth() {
        return 64;
    }

    @Override
    public boolean onInteract(ObjectPlayer player) {
        if (getCart() == null) {
            return false;
        }
        getCart().setPlayer(player);
        state.getObjects().remove(player);
        return true;
    }

    public int getFloor() {
        return floor;
    }

    public ObjectElevatorCart getCart() {
        return cart;
    }

    public void setCart(ObjectElevatorCart cart) {
        this.cart = cart;
        if (cart != null) {
            crowds.stream().forEach((crowd) -> {
                crowd.cartArrived(this);
            });
        }
    }

    public void registerCrowd(ObjectCrowd crowd) {
        crowds.add(crowd);
    }

    public boolean isEscapeElevator() {
        return escapeElevator;
    }

    @Override
    public int getDepth() {
        return 10;
    }

    public String getSystem() {
        return system;
    }

}
