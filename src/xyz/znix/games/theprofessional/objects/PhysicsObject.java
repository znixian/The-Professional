/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public abstract class PhysicsObject extends BasicObject {

    private double vspeed;
    protected boolean isOnFloor;

    public PhysicsObject(IngameState state, MapObj obj) {
        super(state, obj);
    }

    public PhysicsObject(IngameState state, String name) {
        super(state, name);
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        // gravity
        vspeed = Math.min(15, vspeed + 0.5);

        // how far can we move
        int vmovement = moveAtMaxY((int) vspeed);

        // actually move
        y += vmovement;

        // if we can't move but should be, stop
        if (vmovement == 0 && Math.abs(vspeed) >= 1) {
            vspeed = 0;
        }

        // are we on the floor
        isOnFloor = !canMoveTo(getX(), getY() + 1);

        // if so, stop
        if (isOnFloor) {
            vspeed = 0;
        }
    }
}
