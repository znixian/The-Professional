/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import java.util.Iterator;
import java.util.List;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectPlayer extends PhysicsObject {

    private boolean wasInteractDownLast;
    private boolean wasShooting;

    public ObjectPlayer(IngameState state, MapObj obj) {
        super(state, obj);
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        super.update(context, delta);

        // move right
        if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
            x += moveAtMaxX(3);
        }

        // move left
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            x += moveAtMaxX(-3);
        }

        // activate objects
        if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
            if (!wasInteractDownLast && isOnFloor) {
                // find only the in-range objects
                Iterator<BasicObject> ite = state.getObjects().stream().filter((obj) -> {
                    BasicObject oobj = (BasicObject) obj;
                    if (oobj.isInside(getCenterX(), getCenterY())) {
                        return true;
                    }
                    return isInside(oobj.getCenterX(), oobj.getCenterY());
                }).iterator();

                for (; ite.hasNext();) {
                    BasicObject next = ite.next();
                    if (next.onInteract(this)) {
                        break;
                    }
                }
            }

            wasInteractDownLast = true;
        } else {
            wasInteractDownLast = false;
        }

        // update the follow cam
        if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            state.setFollowX(getX() + Mouse.getX() - context.getWidth() / 2);
            state.setFollowY(getY() - Mouse.getY() + context.getHeight() / 2);
        } else {
            state.setFollowX(getX());
            state.setFollowY(getY());
        }
        state.registerPlayerForFrame(this);

        if (Mouse.isButtonDown(1) && Mouse.isButtonDown(0)) {
            if (!wasShooting) {
                int mx = Mouse.getX() + state.getCamX();
                int my = context.getHeight() - Mouse.getY() + state.getCamY();
                List<BasicObject> hit = state.raytrace(
                        getCenterX(), getCenterY(), mx, my, this);

                ShotResult res = new ShotResult();
                res.power = 100;

                for (BasicObject obj : hit) {
                    res.interacted = false;
                    obj.onShoot(res);
                    if (res.power <= 0) {
                        break;
                    }
                }
            }
            wasShooting = true;
        } else {
            wasShooting = false;
        }
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        state.getSheet().getSubImage(3, 1).draw((int) x, (int) y);

        if (Mouse.isButtonDown(1)) {
            g.drawLine(
                    getCenterX(),
                    getCenterY(),
                    Mouse.getX() + state.getCamX(),
                    context.getHeight() - Mouse.getY() + state.getCamY());
        }
    }

    public static class ShotResult {

        /**
         * The power of the shot. 100 for a pistol. Set this to 0 to block the
         * shot.
         */
        public int power;

        /**
         * Set to true if the hit object interacted with the bullet in some way
         */
        public boolean interacted;

        /**
         *
         * Absorb a certain amount of power, and return true if this object can
         * be broken
         *
         * @param power The power to absorb
         * @return Does this shot have enough power to break the given object.
         */
        public boolean absorbPower(int power) {
            interacted = true;
            if (this.power >= power) {
                this.power -= power;
                return true;
            } else {
                this.power = 0;
                return false;
            }
        }
    }
}
