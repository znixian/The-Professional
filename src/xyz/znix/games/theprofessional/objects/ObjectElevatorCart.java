/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.games.theprofessional.objects;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.GUtils;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter
 */
public class ObjectElevatorCart extends BasicObject {

    private final ObjectElevator starting;
    private int floor;
    private boolean goingUp;
    private boolean goingUpSet;
    private ObjectElevator next;
    private ObjectElevator at;
    private int stopTimer;
    private int totalDistance;
    private double speed;
    private ObjectPlayer player;
    private boolean hasCrowd;
    private String system;
    private boolean lightsEnabled = true;
    private boolean sentToMove;
    private boolean interactKeyDown;
    private boolean doorsOpenManually;

    public ObjectElevatorCart(IngameState state, String system, ObjectElevator starting) {
        super(state, "cart " + system);
        this.starting = starting;
        this.system = system;
    }

    @Override
    public void init(Context context) throws SlickException {
        setX(starting.getX());
        setY(starting.getY());
        floor = starting.getFloor();
    }

    @Override
    public int getHeight() {
        return 64;
    }

    @Override
    public int getWidth() {
        return 64;
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        updatePlayer();

        if (next == null) {
            if (!sentToMove && isPlayerControl()) {
                return;
            }
            stopTimer++;
            if (stopTimer < 60) {
                return;
            }
            goingUp = goingUpSet;
            int newFloor = floor;
            if (goingUp) {
                newFloor++;
            } else {
                newFloor--;
            }
            next = starting.getElevatorAtFloor(newFloor);
            if (next == null) {
                goingUpSet = !goingUp;
                return;
            } else {
                sentToMove = false;
                doorsOpenManually = false;
                floor = newFloor;
                totalDistance = distanceToNextStop();
                speed = 0;
                if (at != null) {
                    at.setCart(null);
                }
                at = null;
                setLightsEnabled(true);
            }
        }

        double xdiff = next.getX() - getX();
        double ydiff = next.getY() - getY();

        if (xdiff == 0 && ydiff == 0) {
            at = next;
            at.setCart(this);
            next = null;
            stopTimer = 0;
            return;
        }

        if (distanceToNextStop() > totalDistance / 2) {
            speed += 0.1;
        } else {
            speed -= 0.1;
        }
        if (speed < 0.3) {
            speed = 0.3;
        }

        if (xdiff > speed) {
            xdiff = speed;
        }
        if (xdiff < -speed) {
            xdiff = -speed;
        }
        if (ydiff > speed) {
            ydiff = speed;
        }
        if (ydiff < -speed) {
            ydiff = -speed;
        }

        x += xdiff;
        y += ydiff;
    }

    private int distanceToNextStop() {
        int xdiff = next.getX() - getX();
        int ydiff = next.getY() - getY();
        xdiff = Math.abs(xdiff);
        ydiff = Math.abs(ydiff);
        return Math.max(xdiff, ydiff);
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
//        g.setColor(Color.yellow);
//        g.drawRect(getX(), getY(), getWidth(), getHeight());
        if (isLightsEnabled()) {
            GUtils.drawMultitileSprite(this, state.getSheet(), 6, 3);
        } else {
            GUtils.drawMultitileSprite(this, state.getSheet(), 4, 3);
        }

        if (hasCrowd()) {
            state.getSheet().getSubImage(0, 3).draw(
                    getX() + getWidth() / 2 - 32 / 2,
                    getY() + getHeight() - 32 - 1);
        }
        if (player != null) {
            state.getSheet().getSubImage(3, 1).draw((int) x,
                    (int) y + getHeight() - player.getHeight() - 1);
        }
    }

    public void setPlayer(ObjectPlayer player) {
        interactKeyDown = player != null;
        this.player = player;
    }

    private void updatePlayer() {
        if (player != null) {
            // check if we can escape
            if (at != null && at.isEscapeElevator()
                    && state.getLevel().isFinished()) {
                player = null;
                state.getEngine().removeState();
                return;
            }

            // update the follow cam
            state.setFollowX(getX());
            state.setFollowY(getY() + getHeight() - player.getHeight());

            // can the controls work
            boolean canElevatorControlsWork = isPlayerControl();

            if (canElevatorControlsWork) {
                boolean wKey = Keyboard.isKeyDown(Keyboard.KEY_W);
                if (wKey && !interactKeyDown) {
                    goingUpSet = true;
                    sentToMove = true;
                } else if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
                    goingUpSet = false;
                    sentToMove = true;
                } else if (Keyboard.isKeyDown(Keyboard.KEY_E)) {
                    doorsOpenManually = true;
                }
                if (!wKey) {
                    interactKeyDown = false;
                }
            }

            if ((Keyboard.isKeyDown(Keyboard.KEY_A)
                    || Keyboard.isKeyDown(Keyboard.KEY_D))
                    && at != null) {
                player.setX((int) x);
                player.setY((int) y + getHeight() - player.getHeight());
                state.getObjects().add(player);
                player = null;
            }
        }
    }

    public boolean hasCrowd() {
        return hasCrowd;
    }

    public boolean isStopped() {
        return at != null;
    }

    public boolean areDoorsOpen() {
        return isStopped() && (!isPlayerControl() || doorsOpenManually);
    }

    public boolean isPlayerControl() {
        return player != null && !hasCrowd();
    }

    public void setHasCrowd(boolean hasCrowd) {
        this.hasCrowd = hasCrowd;
    }

    @Override
    public int getDepth() {
        return 20;
    }

    public Object getSystem() {
        return system;
    }

    public boolean isLightsEnabled() {
        return lightsEnabled;
    }

    public void setLightsEnabled(boolean lightsEnabled) {
        this.lightsEnabled = lightsEnabled;
    }

}
