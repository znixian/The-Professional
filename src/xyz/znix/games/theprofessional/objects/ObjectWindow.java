/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectWindow extends BasicObject {

    private final int height;

    public ObjectWindow(IngameState state, MapObj obj) {
        super(state, obj);
        height = (int) (obj.getHeight() / 32);
    }

    @Override
    public int getHeight() {
        return super.getHeight() * height;
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        for (int i = 0; i < height; i++) {
            state.getSheet().getSubImage(0, 1).draw((int) x, (int) y + i * 32);
        }
    }

    @Override
    public void onShoot(ObjectPlayer.ShotResult result) {
        if (result.absorbPower(40)) {
            state.getObjects().remove(this);
        }
    }

    @Override
    public boolean canSeeThrough(BasicObject object) {
        int maxDist = 640;
        int xd = object.getCenterX() - getCenterX();
        int yd = object.getCenterY() - getCenterY();
        return xd * xd + yd * yd < maxDist * maxDist;
    }

    @Override
    public boolean canMoveThrough(BasicObject object) {
        return false;
    }
}
