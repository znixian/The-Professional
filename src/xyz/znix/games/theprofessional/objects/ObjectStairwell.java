/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import java.util.Optional;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectStairwell extends BasicObject {

    private final String system;
    private final boolean topType;

    public ObjectStairwell(MapObj object, IngameState state) {
        super(state, object);
        system = object.getProperties().getProperty("system");
        topType = object.getProperties().getProperty("Type").equals("top");
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        Image sprite;
        if (topType) {
            sprite = state.getSheet().getSubImage(1, 2);
        } else {
            sprite = state.getSheet().getSubImage(1, 1);
        }
        sprite.draw((int) x, (int) y);
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
    }

    private ObjectStairwell getOtherStairs() {
        Optional<ObjectStairwell> obj = state.getObjects().stream()
                .filter((o) -> (o instanceof ObjectStairwell))
                .map((t) -> (ObjectStairwell) t)
                .filter((t) -> t.system.equals(system))
                .filter((t) -> t != this).findAny();
        return obj.orElse(null);
    }

    @Override
    public int getHeight() {
        return 64;
    }

    @Override
    public int getWidth() {
        return 64;
    }

    @Override
    public boolean onInteract(ObjectPlayer player) {
        ObjectStairwell stairs = getOtherStairs();
        player.setX(stairs.getX());
        player.setY(stairs.getY());
        return true;
    }

}
