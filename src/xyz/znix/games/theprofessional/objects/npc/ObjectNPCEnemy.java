/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects.npc;

import java.util.List;
import xyz.znix.games.theprofessional.objects.*;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectNPCEnemy extends PhysicsObject {

    private boolean facingRight;

    public ObjectNPCEnemy(IngameState state, MapObj obj) {
        super(state, obj);
        facingRight = "right".equals(obj.getProperties().getProperty("direction"));
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
        super.update(context, delta);

        tryShoot();
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        state.getSheet().getSubImage(3, 1).draw((int) x, (int) y);
    }

    @Override
    public void onShoot(ObjectPlayer.ShotResult result) {
        result.absorbPower(20);
        state.getObjects().remove(this);
        state.eventOccured(new NPCDiedEvent(this));
    }

    private void tryShoot() {
        ObjectPlayer player = state.getPlayer();

        // does the player exist, and is on the correct side of us?
        if (player != null
                && ((getX() < player.getX()) == facingRight)) {
            // try a quick raytrace
            boolean blocked = state.raytraceWalls(
                    getCenterX(), getCenterY(),
                    player.getCenterX(), player.getCenterY());

            if (blocked) {
                return;
            }
            List<BasicObject> raytrace = state.raytrace(
                    getCenterX(), getCenterY(),
                    player.getCenterX(), player.getCenterY(), player);

            for (BasicObject basicObject : raytrace) {
                if (!basicObject.canSeeThrough(this)) {
                    return;
                }
            }
            state.getObjects().remove(player);
        }
    }

    public static class NPCDiedEvent implements IngameState.Event {

        private final ObjectNPCEnemy npc;

        public NPCDiedEvent(ObjectNPCEnemy npc) {
            this.npc = npc;
        }

        public ObjectNPCEnemy getNpc() {
            return npc;
        }
    }
}
