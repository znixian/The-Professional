/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional.objects;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.games.theprofessional.gui.IngameState;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.tiled.MapObj;

/**
 *
 * @author Campbell Suter
 */
public class ObjectValuable extends BasicObject {

    public ObjectValuable(IngameState state, MapObj obj) {
        super(state, obj);
    }

    @Override
    public void update(Context context, int delta) throws SlickException {
    }

    @Override
    public void render(Context context, Graphics g) throws SlickException {
        state.getSheet().getSubImage(0, 2).draw((int) x, (int) y);
    }

    @Override
    public boolean canMoveThrough(BasicObject object) {
        return (!(object instanceof ObjectPlayer));
    }

    @Override
    public void collideWith(BasicObject object) {
        if (object instanceof ObjectPlayer) {
            state.eventOccured(new ValuableStolenEvent());
            state.getObjects().remove(this);
        }
    }

    public class ValuableStolenEvent implements IngameState.Event {

        public ValuableStolenEvent() {
        }

        public String getName() {
            return ObjectValuable.this.getName();
        }
    }
}
