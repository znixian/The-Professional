/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package xyz.znix.games.theprofessional;

import org.newdawn.slick.SpriteSheet;
import xyz.znix.games.theprofessional.objects.BasicObject;

/**
 * Graphical utilities
 *
 * @author Campbell Suter
 */
public class GUtils {

    private GUtils() {
    }

    public static void drawMultitileSprite(BasicObject obj, SpriteSheet sheet, int tilex, int tiley) {
        // sheet tile size
        int sheetWidth = sheet.getWidth() / sheet.getHorizontalCount();
        int sheetHeight = sheet.getHeight() / sheet.getVerticalCount();

        // size, in tiles, of the object
        int width = obj.getWidth() / sheetWidth;
        int height = obj.getHeight() / sheetHeight;

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                sheet
                        .getSubImage(
                                tilex + x,
                                tiley + y
                        )
                        .draw(
                                obj.getX() + x * sheetWidth,
                                obj.getY() + y * sheetHeight
                        );
            }
        }
    }
}
