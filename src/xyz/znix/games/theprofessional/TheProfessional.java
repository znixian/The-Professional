/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.games.theprofessional;

import xyz.znix.games.theprofessional.gui.LoadingState;
import xyz.znix.slickengine.SlickEngine;

/**
 *
 * @author Campbell Suter
 */
public class TheProfessional {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SlickEngine engine = new SlickEngine("The Professional");
        engine.addState(new LoadingState());
        engine.setShowFPS(false);
        engine.startFullscreen();
    }

}
